package albion.shabani.channelmessaging;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grabasl on 29/01/2018.
 */
public class ChannelContainer {

    public ChannelContainer(){
        channels = new ArrayList<Channel>();
    }

    public ArrayList<Channel> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

    private ArrayList<Channel> channels;

}
