package albion.shabani.channelmessaging;
import java.util.ArrayList;

/**
 * Created by grabasl on 29/01/2018.
 */
public class MessageContainer {
    public MessageContainer() {
        messages = new ArrayList<Message>();
    }

    public ArrayList<Message> getMessages() {

        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    private ArrayList<Message> messages;
}

