package albion.shabani.channelmessaging;

/**
 * Created by shabania on 22/01/2018.
 */
public class Deserialize {

    private String response;
    private String code;
    private String accesstoken;

    public Deserialize() {
    }

    public String getAccesstoken() {

        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }




}
