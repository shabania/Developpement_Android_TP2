package albion.shabani.channelmessaging;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;



/**
 * Created by shabania on 22/01/2018.
 */
public class ChannelActivity extends Activity implements View.OnClickListener, OnDownloadListener {
    private Button sendButton;
    private String channelid;
    private EditText messageText;
    private Handler handler;
    private ListView messagesListView;


    public ChannelActivity() {
        handler = null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channelactivity);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                channelid= null;
            } else {
                channelid= extras.getString("channelID");
            }
        } else {
            channelid= (String) savedInstanceState.getSerializable("channelID");
        }

        sendButton = (Button) findViewById(R.id.buttonSend);
        messageText = (EditText) findViewById(R.id.editTextMessage);
        messagesListView = (ListView) findViewById(R.id.listViewMessages);


        sendButton.setOnClickListener(this);

        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                HttpPostHandler post = new HttpPostHandler();
                post.addOnDownloadListener(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete(String downloadedContent) {

                        Gson gson =new Gson();
                        MessageContainer messages = gson.fromJson(downloadedContent,MessageContainer.class);

                        CustomArrayAdapterMessage adapter = new CustomArrayAdapterMessage(getApplicationContext(),messages.getMessages());
                        messagesListView.setAdapter(adapter);

                    }

                    @Override
                    public void onDownloadError(String error) {
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    }
                });
                HashMap<String,String> hm = new HashMap<String,String>();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                hm.put("accesstoken",prefs.getString("AccessToken",""));
                hm.put("channelid",channelid);

                post.setHttpPostHandler(getApplicationContext());
                post.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=getmessages",hm));

                handler.postDelayed(this, 1000);
            }
        };

        handler.postDelayed(r, 1000);




    }

    @Override
    public void onClick(View v) {

        HttpPostHandler post = new HttpPostHandler();
        post.addOnDownloadListener(this);
        HashMap<String,String> hm = new HashMap<String,String>();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        hm.put("accesstoken",prefs.getString("AccessToken",""));
        hm.put("channelid",channelid);
        hm.put("message",messageText.getText().toString());

        post.setHttpPostHandler(getApplicationContext());
        post.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=sendmessage",hm));


    }

    @Override
    public void onDownloadComplete(String downloadedContent) {
        Toast.makeText(getApplicationContext(), downloadedContent, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDownloadError(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }
}
