package albion.shabani.channelmessaging;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by grabasl on 19/01/2018.
 */
public class ChannelListActivity extends Activity implements OnDownloadListener, AdapterView.OnItemClickListener {

    private ListView channels;
    private ChannelContainer channelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channellistactivity);

        channelList = new ChannelContainer();

        channels = (ListView) findViewById(R.id.listView);


        HttpPostHandler post = new HttpPostHandler();
        post.addOnDownloadListener(this);
        HashMap<String,String> hm = new HashMap<String,String>();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        hm.put("accesstoken",prefs.getString("AccessToken",""));

        post.setHttpPostHandler(getApplicationContext());
        post.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=getchannels",hm));

        channels.setOnItemClickListener(this);
    }

    @Override
    public void onDownloadComplete(String downloadedContent) {
        Gson gson = new Gson();
        channelList = gson.fromJson(downloadedContent, ChannelContainer.class);
        CustomArrayAdapter adapter = new CustomArrayAdapter(this.getApplicationContext(),channelList.getChannels());
        channels.setAdapter(adapter);
    }

    @Override
    public void onDownloadError(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent myIntent = new Intent(getApplicationContext(),ChannelActivity.class);
        myIntent.putExtra("channelID",channelList.getChannels().get(position).getChannelID());
        startActivity(myIntent);
    }
}
