package albion.shabani.channelmessaging;

/**
 * Created by shabania on 19/01/2018.
 */
interface OnDownloadListener {

    void onDownloadComplete(String downloadedContent);

    void onDownloadError(String error);
}